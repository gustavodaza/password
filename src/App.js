import React, { Component } from 'react';
import './App.css';


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      value: ''
    };
    this.submit = this.submit.bind(this);
  }


  submit(event){
    event.preventDefault();
    this.setState({ value: this.element.value });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-4">
            <form onChange={this.submit}>
            <div className="form-group">
              <label>Password:</label>
              <input type="password" className="form-control" placeholder="Enter Password" ref={el => this.element = el}/>
          </div>
        </form>
      </div>
      <div className="col-4">
            <form>
            <div className="form-group">
              <label>Password:</label>
              <input value={this.state.value} type="password" className="form-control" disabled></input>
                Show Password: <input type="checkbox"></input>
          </div>
        </form>
      </div>
   </div>
 </div>

    );
  }
}

export default App;
